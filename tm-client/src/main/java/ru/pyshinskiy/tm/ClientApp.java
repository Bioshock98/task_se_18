package ru.pyshinskiy.tm;

import ru.pyshinskiy.tm.bootstrap.Bootstrap;

import javax.enterprise.inject.se.SeContainerInitializer;

public final class ClientApp {

    public static void main(String[] args) throws Exception {
        SeContainerInitializer.newInstance()
                .addPackages(ClientApp.class).initialize()
                .select(Bootstrap.class).get().start();
    }
}
