package ru.pyshinskiy.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.service.SessionService;
import ru.pyshinskiy.tm.service.TerminalService;

import javax.inject.Inject;

public abstract class AbstractCommand {

    @Inject
    protected TerminalService terminalService;

    @Inject
    protected SessionService sessionService;

    public boolean isAllowed() {
        return sessionService.getSessionDTO() != null;
    }

    @NotNull
    public abstract String command();

    @NotNull
    public abstract String description();

    public abstract void execute() throws Exception;
}
