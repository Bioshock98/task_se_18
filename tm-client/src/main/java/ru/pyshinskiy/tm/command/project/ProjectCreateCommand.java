package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.ProjectDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import javax.inject.Inject;

import static ru.pyshinskiy.tm.util.date.DateUtil.parseDateFromString;
import static ru.pyshinskiy.tm.util.date.DateUtil.toXMLGregorianCalendar;

public final class ProjectCreateCommand extends AbstractCommand {

    @Inject
    private IProjectEndpoint projectEndpoint;

    @Override
    @NotNull
    public String command() {
        return "project_create";
    }

    @Override
    @NotNull
    public String description() {
        return "create new project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME");
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(sessionService.getSessionDTO().getUserId());
        project.setName(terminalService.nextLine());
        System.out.println("ENTER PROJECT DESCRIPTION");
        project.setDescription(terminalService.nextLine());
        System.out.println("ENTER START DATE");
        project.setStartDate(toXMLGregorianCalendar(parseDateFromString(terminalService.nextLine())));
        System.out.println("ENTER END DATE");
        project.setFinishDate(toXMLGregorianCalendar(parseDateFromString(terminalService.nextLine())));
        projectEndpoint.persistProject(sessionService.getSessionDTO(), project);
        System.out.println("[OK]");
    }
}
