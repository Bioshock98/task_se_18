package ru.pyshinskiy.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.IUserEndpoint;
import ru.pyshinskiy.tm.api.endpoint.ServerInfo;
import ru.pyshinskiy.tm.command.AbstractCommand;

import javax.inject.Inject;

public final class ServerInfoCommand extends AbstractCommand {

    @Inject
    private IUserEndpoint userEndpoint;

    @Override
    public @NotNull String command() {
        return "server_info";
    }

    @Override
    public @NotNull String description() {
        return "show server information";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ServerInfo serverInfo = userEndpoint.getServerInfo(sessionService.getSessionDTO());
        System.out.println("[Server-info]");
        System.out.println("HOST: " + serverInfo.getHost());
        System.out.println("PORT: " + serverInfo.getPort());
        System.out.println("[OK]");
    }
}
