package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.TaskDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import javax.inject.Inject;
import java.util.List;

import static ru.pyshinskiy.tm.util.date.DateUtil.parseDateFromString;
import static ru.pyshinskiy.tm.util.date.DateUtil.toXMLGregorianCalendar;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskEditCommand extends AbstractCommand {

    @Inject
    private ITaskEndpoint taskEndpoint;

    @Override
    @NotNull
    public String command() {
        return "task_edit";
    }

    @Override
    @NotNull
    public String description() {
        return "edit existing task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK EDIT]");
        System.out.println("ENTER TASK ID");
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findAllTasksByUserId(sessionService.getSessionDTO());
        printTasks(tasks);
        final int taskNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String taskId = tasks.get(taskNumber).getId();
        @Nullable final TaskDTO task = taskEndpoint.findOneTaskByUserId(sessionService.getSessionDTO(), taskId);
        if(task == null) {
            throw new Exception("task doesn't exist");
        }
        System.out.println("[ENTER TASK NAME]");
        @NotNull final TaskDTO anotherTask = new TaskDTO();
        anotherTask.setId(task.getId());
        anotherTask.setUserId(task.getUserId());
        anotherTask.setName(terminalService.nextLine());
        System.out.println("ENTER TASK DESCRIPTION");
        anotherTask.setDescription(terminalService.nextLine());
        System.out.println("ENTER START DATE");
        anotherTask.setStartDate(toXMLGregorianCalendar(parseDateFromString(terminalService.nextLine())));
        System.out.println("ENTER FINISH DATE");
        anotherTask.setFinishDate(toXMLGregorianCalendar(parseDateFromString(terminalService.nextLine())));
        taskEndpoint.mergeTask(sessionService.getSessionDTO(), anotherTask);
        System.out.println("[OK]");
    }
}
