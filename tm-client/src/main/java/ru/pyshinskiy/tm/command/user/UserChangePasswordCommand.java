package ru.pyshinskiy.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.IUserEndpoint;
import ru.pyshinskiy.tm.api.endpoint.UserDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import javax.inject.Inject;

public final class UserChangePasswordCommand extends AbstractCommand {

    @Inject
    private IUserEndpoint userEndpoint;

    @Override
    @NotNull
    public String command() {
        return "user_change_password";
    }

    @Override
    @NotNull
    public String description() {
        return "change user password";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER CHANGE PASSWORD");
        System.out.println("ENTER NEW PASSWORD");
        @NotNull final UserDTO user = new UserDTO();
        user.setId(sessionService.getSessionDTO().getUserId());
        user.setPasswordHash(terminalService.nextLine());
        userEndpoint.mergeUser(sessionService.getSessionDTO(), user);
        System.out.println("[OK]");
    }
}
