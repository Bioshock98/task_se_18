package ru.pyshinskiy.tm.bootstrap;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.ISessionEndpoint;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.IUserEndpoint;

import javax.inject.Inject;
import javax.xml.ws.Endpoint;

@NoArgsConstructor
public class Bootstrap {

    @Inject
    private SessionCleaner sessionCleaner;

    @Inject
    private IProjectEndpoint projectEndpoint;

    @Inject
    private ITaskEndpoint taskEndpoint;

    @Inject
    private IUserEndpoint userEndpoint;

    @Inject
    private ISessionEndpoint sessionEndpoint;

    public void start() {
        @NotNull final String portNumber = System.getProperty("server.port");
        Endpoint.publish("http://localhost:" + portNumber + "/projectService?wsdl", projectEndpoint);
        Endpoint.publish("http://localhost:" + portNumber + "/taskService?wsdl", taskEndpoint);
        Endpoint.publish("http://localhost:" + portNumber + "/userService?wsdl", userEndpoint);
        Endpoint.publish("http://localhost:" + portNumber + "/sessionService?wsdl", sessionEndpoint);
        sessionCleaner.setDaemon(true);
        sessionCleaner.start();
    }
}
