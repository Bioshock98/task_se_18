package ru.pyshinskiy.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.ISessionEndpoint;
import ru.pyshinskiy.tm.dto.SessionDTO;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.util.security.SignatureUtil;

import javax.jws.WebMethod;
import javax.jws.WebService;

import static ru.pyshinskiy.tm.constant.AppConst.CICLE;
import static ru.pyshinskiy.tm.constant.AppConst.SALT;

@NoArgsConstructor
@WebService(endpointInterface = "ru.pyshinskiy.tm.api.endpoint.ISessionEndpoint")
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @WebMethod
    @Override
    @Nullable
    public SessionDTO findSession(@NotNull final String id) throws Exception {
        return Session.toSessionDTO(serviceLocator.getSessionService().findOne(id));
    }

    @WebMethod
    @Override
    @Nullable
    public SessionDTO createSession(@NotNull final String login, @NotNull final String password) throws Exception {
        @Nullable final User user = serviceLocator.getUserService().getUserByLogin(login);
        if(user == null) return null;
        if(!user.getPasswordHash().equals(SignatureUtil.sign(password, SALT, CICLE))) return null;
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setUserId(user.getId());
        sessionDTO.setRole(user.getRole());
        @NotNull final Session session = SessionDTO.toSession(serviceLocator, sessionDTO);
        session.setSignature(SignatureUtil.sign(sessionDTO, SALT, CICLE));
        serviceLocator.getSessionService().persist(session);
        return sessionDTO;
    }

    @WebMethod
    @Override
    @NotNull
    public SessionDTO updateSession(@NotNull final SessionDTO sessionDTO) throws Exception {
        @Nullable final Session oldSession = serviceLocator.getSessionService().findOneByUserId(sessionDTO.getUserId(), sessionDTO.getId());
        @NotNull final SessionDTO newSessionDTO = new SessionDTO();
        newSessionDTO.setId(oldSession.getId());
        newSessionDTO.setUserId(oldSession.getUser().getId());
        newSessionDTO.setRole(oldSession.getRole());
        @NotNull final Session newSession = SessionDTO.toSession(serviceLocator, newSessionDTO);
        newSession.setSignature(SignatureUtil.sign(newSessionDTO, SALT, CICLE));
        serviceLocator.getSessionService().merge(newSession);
        return newSessionDTO;
    }

    @WebMethod
    @Override
    public void removeSession(@Nullable final String userId, @Nullable final String id) throws Exception {
        serviceLocator.getSessionService().removeByUserId(userId, id);
    }

    @WebMethod
    @Override
    public void removeAllSessions() throws Exception {
        serviceLocator.getSessionService().removeAll();
    }

    @WebMethod
    @Override
    public void removeAllSessionsByUserId(@Nullable final String userId) throws Exception {
        serviceLocator.getSessionService().removeAllByUserId(userId);
    }
}
