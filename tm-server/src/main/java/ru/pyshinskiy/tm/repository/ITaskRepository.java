package ru.pyshinskiy.tm.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryParam;
import org.apache.deltaspike.data.api.Repository;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.entity.Task;

import javax.persistence.QueryHint;
import java.util.List;

@Repository
public interface ITaskRepository extends EntityRepository<Task, String> {

    @Query(value = "select t from Task t where t.user.id = :userId", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    List<Task> findAllByUserId(@QueryParam("userId") String userId);

    @Query(value = "select t from Task t where t.user.id = :userId and t.project.id = :projectId", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    List<Task> findAllByProjectId(@QueryParam("userId") @NotNull final String userId, @QueryParam("projectId") @NotNull final String projectId) throws Exception;

    @Query("delete from Task t")
    void removeAll() throws Exception;

    @Query(value = "select t from Task t where t.id = :id and t.user.id = :userId", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    Task findOneByUserId(@QueryParam("userId") @NotNull final String userId, @QueryParam("id") @NotNull final String id) throws Exception;

    @Query(value = "select t from Task t where t:user.id = :userId and t.name = :name", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    List<Task> findByName(@QueryParam("userId") @NotNull final String userId, @QueryParam("name") @NotNull final String name);

    @Query(value = "select t from Task t where t:user.id = :userId and t.description = :description", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    List<Task> findByDescription(@QueryParam("userId") @NotNull final String userId, @QueryParam("description") @NotNull final String description);

    @Query("delete from Task t where t.user.id = :userId and t.id = :id")
    void removeByUserId(@QueryParam("userId") @NotNull final String userId, @QueryParam("id") @NotNull final String id) throws Exception;

    @Query("delete from Task t where t.user.id = :userId")
    void removeAllByUserId(@QueryParam("userId") @NotNull final String userId) throws Exception;

    @Query(value = "select t from Task t where t.user.id= :userId order by t.createTime", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    List<Task> sortByCreateTime(@QueryParam("userId") @NotNull final String userId) throws Exception;

    @Query(value = "select t from Task t where t.user.id= :userId order by t.startDate", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    List<Task> sortByStartDate(@QueryParam("userId") @NotNull final String userId) throws Exception;

    @Query(value = "select t from Task t where t.user.id= :userId order by t.finishDate", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    List<Task> sortByFinishDate(@QueryParam("userId") @NotNull final String userId) throws Exception;

    @Query(value = "select t from Task t where t.user.id= :userId order by t.status", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    List<Task> sortByStatus(@QueryParam("userId") @NotNull final String userId) throws Exception;
}
