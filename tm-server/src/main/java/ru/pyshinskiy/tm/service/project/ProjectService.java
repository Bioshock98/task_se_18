package ru.pyshinskiy.tm.service.project;

import org.apache.deltaspike.jpa.api.transaction.Transactional;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.repository.IProjectRepository;
import ru.pyshinskiy.tm.service.wbs.AbstractWBSService;

import javax.inject.Inject;
import javax.persistence.NoResultException;
import java.util.List;

@Transactional
public class ProjectService extends AbstractWBSService<Project> implements IProjectService {

    @Inject
    private IProjectRepository projectRepository;

    @Nullable
    @Override
    public Project findOneByUserId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) return null;
        if(userId == null || userId.isEmpty()) return null;
        @Nullable final Project project;
        try {
            project = projectRepository.findOneByUserId(userId, id);
        }
        catch (NoResultException e) {
            return null;
        }
        return project;
    }

    @Override
    @NotNull
    public List<Project> findAllByUserId(@Nullable final String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final List<Project> projects = projectRepository.findAllByUserId(userId);
        return projects;
    }

    @Override
    public void removeByUserId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("project id is empty or null");
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        projectRepository.removeByUserId(userId, id);
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        projectRepository.removeAllByUserId(userId);
    }

    @Override
    @NotNull
    public List<Project> findByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        if(name == null || name.isEmpty()) throw new Exception("name is empty or null");
        @NotNull final List<Project> projects = projectRepository.findByName(userId, name);
        return projects;
    }

    @Override
    @NotNull
    public List<Project> findByDescription(@Nullable final String userId, @Nullable final String description) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        if(description == null || description.isEmpty()) throw new Exception("description is empty or null");
        @NotNull final List<Project> projects = projectRepository.findByDescription(userId, description);
        return projects;
    }

    @Override
    @NotNull
    public List<Project> sortByCreateTime(@Nullable final String userId, final int direction) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final List<Project> projects = projectRepository.sortByCreateTime(userId);
        return projects;
    }

    @Override
    public @NotNull List<Project> sortByStartDate(@Nullable final String userId, final int direction) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final List<Project> projects = projectRepository.sortByStartDate(userId);
        return projects;
    }

    @Override
    @NotNull
    public List<Project> sortByFinishDate(@Nullable final String userId, final int direction) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final List<Project> projects = projectRepository.sortByFinishDate(userId);
        return projects;
    }

    @Override
    @NotNull
    public List<Project> sortByStatus(@Nullable final String userId, final int direction) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final List<Project> projects = projectRepository.sortByStatus(userId);
        return projects;
    }

    @Override
    @Nullable
    public Project findOne(@Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) return null;
        @Nullable final Project project = projectRepository.findBy(id);
        return project;
    }

    @Override
    @NotNull
    public List<Project> findAll() {
        @NotNull final List<Project> projects = projectRepository.findAll();
        return projects;
    }

    @Override
    public void persist(@Nullable final Project project) throws Exception {
        if(project == null) throw new Exception("invalid project");
        projectRepository.save(project);
    }

    @Override
    public void merge(@Nullable final Project project) throws Exception {
        if(project == null) throw new Exception("invalid project");
        projectRepository.save(project);
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if(id == null) throw new Exception("invalid project id");
        @Nullable final Project project = findOne(id);
        if(project == null) return;
        projectRepository.remove(project);
    }

    @Override
    public void removeAll() throws Exception{
        projectRepository.removeAll();
    }
}
