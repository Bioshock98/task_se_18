package ru.pyshinskiy.tm.service.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.service.wbs.IAbstractVBSService;

import java.util.List;

public interface ITaskService extends IAbstractVBSService<Task> {

    @NotNull
    List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception;
}
