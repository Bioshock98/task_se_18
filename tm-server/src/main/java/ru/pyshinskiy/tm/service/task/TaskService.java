package ru.pyshinskiy.tm.service.task;

import org.apache.deltaspike.jpa.api.transaction.Transactional;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.repository.ITaskRepository;
import ru.pyshinskiy.tm.service.wbs.AbstractWBSService;

import javax.inject.Inject;
import javax.persistence.NoResultException;
import java.util.List;

@Transactional
public class TaskService extends AbstractWBSService<Task> implements ITaskService {

    @Inject
    private ITaskRepository taskRepository;

    @Nullable
    @Override
    public Task findOneByUserId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("task id is empty or null");
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @Nullable final Task task;
        try {
            task = taskRepository.findOneByUserId(userId, id);
        }
        catch (NoResultException e) {
            return null;
        }
        return task;
    }

    @Override
    @NotNull
    public List<Task> findAllByUserId(@Nullable final String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final List<Task> tasks = taskRepository.findAllByUserId(userId);
        return tasks;
    }

    @Override
    public void removeByUserId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("task id is empty or null");
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        taskRepository.removeByUserId(userId, id);
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        taskRepository.removeAllByUserId(userId);
    }

    @Override
    @NotNull
    public List<Task> findByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        if(name == null || name.isEmpty()) throw new Exception("name is empty or null");
        @NotNull final List<Task> tasks = taskRepository.findByName(userId, name);
        return tasks;
    }

    @Override
    @NotNull
    public List<Task> findByDescription(@Nullable final String userId, @Nullable final String description) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        if(description == null || description.isEmpty()) throw new Exception("description is empty or null");
        @NotNull final List<Task> tasks = taskRepository.findByDescription(userId, description);
        return tasks;
    }

    @Override
    @NotNull
    public List<Task> sortByCreateTime(@Nullable final String userId, final int direction) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final List<Task> tasks = taskRepository.sortByCreateTime(userId);
        return tasks;
    }

    @Override
    public @NotNull List<Task> sortByStartDate(@Nullable final String userId, final int direction) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final List<Task> tasks = taskRepository.sortByStartDate(userId);
        return tasks;
    }

    @Override
    @NotNull
    public List<Task> sortByFinishDate(@Nullable final String userId, final int direction) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final List<Task> tasks = taskRepository.sortByFinishDate(userId);
        return tasks;
    }

    @Override
    @NotNull
    public List<Task> sortByStatus(@Nullable final String userId, final int direction) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final List<Task> tasks = taskRepository.sortByStatus(userId);
        return tasks;
    }

    @Override
    @Nullable
    public Task findOne(@Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("task id is empty or null");
        @Nullable final Task task = taskRepository.findBy(id);
        return task;
    }

    @Override
    @NotNull
    public List<Task> findAll() {
        @NotNull final List<Task> tasks = taskRepository.findAll();
        return tasks;
    }

    @Override
    public void persist(@Nullable final Task task) throws Exception {
        if(task == null) throw new Exception("invalid task");
        taskRepository.save(task);
    }

    @Override
    public void merge(@Nullable final Task task) throws Exception {
        if(task == null) throw new Exception("invalid task");
        taskRepository.save(task);
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if(id == null) throw new Exception("invalid task id");
        @Nullable final Task task = findOne(id);
        if(task == null) throw new Exception("task is null");
        taskRepository.remove(task);
    }

    @Override
    public void removeAll() throws Exception {
        taskRepository.removeAll();
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String taskId) throws Exception {
        if(userId == null || taskId == null) throw new Exception("one of the parameters passed is zero");
        @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(userId, taskId);
        return tasks;
    }
}
